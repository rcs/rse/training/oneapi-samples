#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module2 -- SYCL Program Structure sample - 6 of 7 complex_mult.cpp
#clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda complex_mult.cpp -o bin/complex_mult -I ./include
#bin/complex_mult
chmod 755 q
make clean
make all
make run

