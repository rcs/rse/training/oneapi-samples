#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module2 -- SYCL Program Structure sample - 5 of 7 custom_device_sample.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/custom_device_sample.cpp
if [ $? -eq 0 ]; then ./a.out; fi

