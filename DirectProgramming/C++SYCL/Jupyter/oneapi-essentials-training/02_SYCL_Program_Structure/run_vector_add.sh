#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module2 -- SYCL Program Structure sample - 7 of 7 vector_add.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/vector_add.cpp
if [ $? -eq 0 ]; then ./a.out; fi

