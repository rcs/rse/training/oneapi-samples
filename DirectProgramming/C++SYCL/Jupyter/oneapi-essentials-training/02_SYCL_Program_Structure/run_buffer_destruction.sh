#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module2 -- SYCL Program Structure sample - 4 of 7 buffer_destruction2.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/buffer_destruction2.cpp
if [ $? -eq 0 ]; then ./a.out; fi

