#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module2 -- SYCL Program Structure sample - 2 of 7 buffer_sample.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/buffer_sample.cpp
if [ $? -eq 0 ]; then ./a.out; fi

