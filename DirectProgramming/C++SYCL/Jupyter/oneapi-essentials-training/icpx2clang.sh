#!/bin/sh

replace_icpx() {
  sed -i 's/icpx -fsycl/clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda/' "$1"
}

fix_env() {
  sed -i 's#source /opt/intel/oneapi/setvars.sh#source ../set_env.sh#' "$1"
}

find -name 'run_*.sh' | while read f; do 
  replace_icpx "$f"
  fix_env "$f"
done
