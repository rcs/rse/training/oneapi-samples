#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) "is compiling Welcome Module-- 1 of 1 hello.cpp"
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda src/hello.cpp -o src/hello
if [ $? -eq 0 ]; then src/hello; fi

