#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module12 -- SYCL Atomics Local Memory - 4 of 5 matrixmul_16x16_localmem.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/matrixmul_16x16_localmem.cpp 
if [ $? -eq 0 ]; then ./a.out; fi

