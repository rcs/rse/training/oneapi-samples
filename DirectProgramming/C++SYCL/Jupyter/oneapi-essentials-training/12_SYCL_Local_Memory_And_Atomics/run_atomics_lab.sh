#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module12 -- SYCL Atomics Local Memory - 6 of 6 atomics_lab.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/atomics_lab.cpp 
if [ $? -eq 0 ]; then ./a.out; fi

