#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module12 -- SYCL Atomics Local Memory - 3 of 5 localmem_info.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/localmem_info.cpp 
if [ $? -eq 0 ]; then ./a.out; fi

