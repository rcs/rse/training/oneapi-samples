#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda src/iso3dfd.cpp src/utils.cpp src/iso3dfd_kernels.cpp -o iso3dfd
./iso3dfd 256 256 256 8 8 8 20 sycl gpu

