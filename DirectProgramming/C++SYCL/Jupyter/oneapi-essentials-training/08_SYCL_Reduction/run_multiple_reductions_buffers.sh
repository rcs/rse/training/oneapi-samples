#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module8 -- SYCL Reduction - 6 of 8 multiple_reductions_buffers.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/multiple_reductions_buffers.cpp 
if [ $? -eq 0 ]; then ./a.out; fi
