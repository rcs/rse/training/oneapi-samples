#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module8 -- SYCL Reduction - 2 of 8 sum_work_group.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/sum_work_group.cpp 
if [ $? -eq 0 ]; then ./a.out; fi

