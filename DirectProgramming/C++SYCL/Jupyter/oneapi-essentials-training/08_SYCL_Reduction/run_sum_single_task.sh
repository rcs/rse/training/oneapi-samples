#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module8 -- SYCL Reduction - 1 of 8 sum_single_task.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/sum_single_task.cpp 
if [ $? -eq 0 ]; then ./a.out; fi

