#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module1 -- oneAPI Intro sample - 1 of 1 simple.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/simple.cpp
if [ $? -eq 0 ]; then ./a.out; fi

