#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module7 -- oneDPL Intro sample - 2 of 5 dpl_sortdouble.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/dpl_sortdouble.cpp -o dpl_sortdouble -w
if [ $? -eq 0 ]; then ./dpl_sortdouble; fi

