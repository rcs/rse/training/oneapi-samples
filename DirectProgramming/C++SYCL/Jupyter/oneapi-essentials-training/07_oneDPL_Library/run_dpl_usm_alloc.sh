#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module7 -- oneDPL Intro sample - 5 of 5 dpl_usm_alloc.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/dpl_usm_alloc.cpp -o dpl_usm_alloc -w
if [ $? -eq 0 ]; then ./dpl_usm_alloc; fi

