#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module9 -- SYCL Advanced Buffers and Accessors sample - 10 of 10 lab_buffers.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/lab_buffers.cpp
if [ $? -eq 0 ]; then ./a.out; fi

