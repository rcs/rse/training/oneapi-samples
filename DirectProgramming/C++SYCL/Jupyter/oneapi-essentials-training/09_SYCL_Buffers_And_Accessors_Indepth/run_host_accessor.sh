#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module9 -- SYCL Advanced Buffers and Accessors sample - 6 of 10 host_accessor_sample.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/host_accessor_sample.cpp
if [ $? -eq 0 ]; then ./a.out; fi

