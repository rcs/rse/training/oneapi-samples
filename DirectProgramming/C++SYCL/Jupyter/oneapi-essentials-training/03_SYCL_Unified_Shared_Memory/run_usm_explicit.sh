#!/bin/bash
source ../set_env.sh > /dev/null 2>&1
/bin/echo "##" $(whoami) is compiling SYCL_Essentials Module3 -- SYCL Unified Shared Memory - 2 of 5 usm_explicit.cpp
clang++ -fsycl -fsycl-targets=nvptx64-nvidia-cuda lab/usm_explicit.cpp 
if [ $? -eq 0 ]; then ./a.out; fi

